import os
import re
import subprocess
from urllib.request import urlretrieve

import httpx
from bs4 import BeautifulSoup


def fetch_url(url: str) -> str:
    """fetch html from url

    :url: TODO
    :returns: TODO

    """
    html = httpx.get(url)
    html.raise_for_status()
    return html.text


def parse(html: str) -> BeautifulSoup:
    """parse html using BeautifulSoup

    :html: str
    :returns: BeautifulSoup

    """
    parser = BeautifulSoup(html, "html.parser")
    return parser


def has_nvim_update(parser: BeautifulSoup) -> bool:
    """parse and check if update is required

    :parser: BeautifulSoup
    :returns: bool

    """
    header = parser.find("div", class_="release-header")
    nvim_version = header.findChild("a").text.strip()
    print(f"Latest version: {nvim_version}")

    # Check current version of neovim
    cmd = subprocess.run(["nv", "--version"], stdout=subprocess.PIPE)
    current_version = cmd.stdout.decode().splitlines()[0].strip()
    print(f"Current version: {current_version}")
    return current_version != nvim_version


def get_download_url(parser: BeautifulSoup) -> str:
    """extract download url from html parser

    :parser: BeautifulSoup
    :returns: str

    """
    url_element = parser.find("a", href=re.compile("\\.appimage$"))
    url = url_element.get("href")
    return f"https://github.com{url}"


def download(url: str):
    filename = url.split("/")[-1]
    if os.path.exists(filename):
        print("File already downloaded")
        return filename
    urlretrieve(url=url, filename=filename)
    print("Download complete.")
    return filename


def post_download(filename: str):
    """post download steps

    :filename: str
    :returns: None

    """
    dst_file = os.path.expanduser("~/.local/bin/nv")
    os.rename(filename, dst_file)
    subprocess.run(["chmod", "+x", dst_file])
    print("Post steps complete")


def main():
    nvim_url = "https://github.com/neovim/neovim/releases/tag/nightly"
    html = fetch_url(nvim_url)
    parser = parse(html)
    if has_nvim_update(parser):
        download_url = get_download_url(parser)
        print(download_url)
        filename = download(download_url)
        post_download(filename)


if __name__ == "__main__":
    main()
